﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Speech_Recognition
{
    public partial class Form_InsertMessage : Form
    {
        public Form_InsertMessage(string a="", int trying=Tring.Shell,string b="", string c="",bool isCtrl=false, bool isShift = false, bool isAlt = false)
        {
            InitializeComponent();
            for (int i = (int)Keys.A; i <= (int)Keys.Z; ++i)
                this.comboBox_key.Items.Add(((char)i).ToString());
            for (int i = 1; i <= 12; ++i)
                this.comboBox_key.Items.Add("F"+i.ToString());
            this.comboBox_key.Items.Add("Left");
            this.comboBox_key.Items.Add("Right");
            this.comboBox_key.Items.Add("Up");
            this.comboBox_key.Items.Add("Down");

            this.checkBox_Ctrl.Checked = isCtrl;
            this.checkBox_Shift.Checked = isShift;
            this.checkBox_Alt.Checked = isAlt;
            this.textBox_key.Text = a;
            if (trying == Tring.Shell)
            {
                this.textBox_shellexec.Text = b;
                this.radioButton_Shell.Checked = true;
            }
            else
            {
                this.comboBox_key.Text = b;
                this.radioButton_key.Checked = true;
            }
            this.comboBox_isSearch.Text = c;

            
        }

        private void button_canel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void button_yes_Click(object sender, EventArgs e)
        {
            if (this.comboBox_isSearch.Text != "" || this.comboBox_key.Text != "")
            {
                this.DialogResult = DialogResult.Yes;
                this.Hide();
            }
        }

        private void Form_InsertMessage_Load(object sender, EventArgs e)
        {
            this.comboBox_isSearch.Text = "模糊识别";
        }

        private void ReRadioButtion()
        {
            if(radioButton_key.Checked)
            {
                this.textBox_shellexec.Enabled = false;
                this.checkBox_Alt.Enabled = this.checkBox_Ctrl.Enabled = this.checkBox_Shift.Enabled = this.comboBox_key.Enabled = true;
            }
            else
            {
                this.textBox_shellexec.Enabled = true;
                this.checkBox_Alt.Enabled = this.checkBox_Ctrl.Enabled = this.checkBox_Shift.Enabled = this.comboBox_key.Enabled = false;
            }
        }

        private void radioButton_key_CheckedChanged(object sender, EventArgs e)
        {
            ReRadioButtion();
        }

        private void radioButton_Shell_CheckedChanged(object sender, EventArgs e)
        {
            ReRadioButtion();
        }
    }
}
