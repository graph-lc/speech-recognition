﻿namespace Speech_Recognition
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button_Stop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_Command = new System.Windows.Forms.Label();
            this.toolTip_Help = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // button_Stop
            // 
            this.button_Stop.BackColor = System.Drawing.Color.Blue;
            this.button_Stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Stop.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_Stop.ForeColor = System.Drawing.Color.White;
            this.button_Stop.Location = new System.Drawing.Point(345, 202);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(67, 27);
            this.button_Stop.TabIndex = 0;
            this.button_Stop.TabStop = false;
            this.button_Stop.Text = "停止";
            this.button_Stop.UseVisualStyleBackColor = false;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.Location = new System.Drawing.Point(26, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(369, 167);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nothing";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Command
            // 
            this.lbl_Command.AutoSize = true;
            this.lbl_Command.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbl_Command.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lbl_Command.ForeColor = System.Drawing.Color.Gray;
            this.lbl_Command.Location = new System.Drawing.Point(307, 207);
            this.lbl_Command.Name = "lbl_Command";
            this.lbl_Command.Size = new System.Drawing.Size(32, 17);
            this.lbl_Command.TabIndex = 2;
            this.lbl_Command.Text = "命令";
            this.lbl_Command.Click += new System.EventHandler(this.lbl_Command_Click);
            this.lbl_Command.MouseEnter += new System.EventHandler(this.lbl_Command_MouseEnter);
            this.lbl_Command.MouseLeave += new System.EventHandler(this.lbl_Command_MouseLeave);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 241);
            this.Controls.Add(this.lbl_Command);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_Stop);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(440, 280);
            this.MinimumSize = new System.Drawing.Size(440, 280);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Speech Recognition";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_Command;
        private System.Windows.Forms.ToolTip toolTip_Help;
    }
}

