﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Speech_Recognition
{
    public partial class Form_Command : Form
    {
        private string this_path = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;

        public Form_Command()
        {
            InitializeComponent();
        }

        private void WriteCommandList()
        {
            StreamWriter w = new StreamWriter(this_path + "command.config", false, Encoding.UTF8);
            foreach(ListViewItem i in this.listView_Command.Items)
            {
                w.WriteLine(i.SubItems[0].Text);
                w.WriteLine(i.SubItems[1].Text.Substring(0, 2) == "运行" ? "shell" : "key");
                w.WriteLine(i.SubItems[1].Text.Substring(3));
                w.WriteLine(i.SubItems[2].Text == "模糊识别" ? "true" : "false");
            }
            w.Close();
        }
        
        private void Form_Command_Load(object sender, EventArgs e)
        {
            this.listView_Command.Columns.Clear();
            this.listView_Command.View = View.Details;

            string[] headText = new string[3] { "关键字", "操作", "决策" };
            int[] headWidth = new int[3] { (this.listView_Command.Width - 84) / 2, (this.listView_Command.Width - 84) / 2, 80 };
            for(int i =0;i<3;++i)
            {
                ColumnHeader h = new ColumnHeader
                {
                    Text = headText[i],
                    Width = headWidth[i]
                };
                this.listView_Command.Columns.Add(h);
            }

            if(File.Exists(this_path + "command.config"))
            {
                StreamReader r = new StreamReader(this_path + "command.config", Encoding.UTF8);
                while(!r.EndOfStream)
                {
                    ListViewItem i = new ListViewItem
                    {
                        Text = r.ReadLine()
                    };

                    string path = r.ReadLine(),mode = r.ReadLine(),searching;
                    if (mode == "true" || mode == "false")//兼容旧版本
                    {
                        searching = mode;
                        mode = "shell";
                    }
                    else searching = r.ReadLine();

                    if (path == "shell")
                        i.SubItems.Add("运行 " + mode);
                    else i.SubItems.Add("按键 " + mode);
                    i.SubItems.Add(searching == "true" ? "模糊识别" : "全匹配");
                    this.listView_Command.Items.Add(i);
                }
                r.Close();
            }
        }

        private void cMenu_Insert_Click(object sender, EventArgs e)
        {
            Form_InsertMessage finsert = new Form_InsertMessage();
            DialogResult r = finsert.ShowDialog();
            if(r == DialogResult.Yes)
            {
                ListViewItem i = new ListViewItem
                {
                    Text = finsert.textBox_key.Text
                };
                i.SubItems.Add(finsert.radioButton_Shell.Checked ? ("运行 " + finsert.textBox_shellexec.Text) : ("按键 " + (finsert.checkBox_Ctrl.Checked ? "Ctrl+" : "") + (finsert.checkBox_Shift.Checked ? "Shift+" : "") + (finsert.checkBox_Alt.Checked ? "Alt+" : "") + finsert.comboBox_key.Text));
                i.SubItems.Add(finsert.comboBox_isSearch.Text);
                this.listView_Command.Items.Add(i);
            }
        }

        private void cMenu_DeleteAll_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("确定要清除所有命令？", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if(r == DialogResult.Yes)
            {
                this.listView_Command.Items.Clear();
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            WriteCommandList();
            this.Close();
        }

        private void cMenu_Opening(object sender, CancelEventArgs e)
        {
            this.cMenu_Delete.Visible = this.cMenu_Delete.Enabled = this.listView_Command.SelectedItems.Count == 1;
        }

        private void cMenu_Delete_Click(object sender, EventArgs e)
        {
            this.listView_Command.Items.Remove(this.listView_Command.SelectedItems[0]);
        }

        private void listView_Command_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.listView_Command.SelectedItems.Count != 1) return;
            Form_InsertMessage finsert = new Form_InsertMessage(this.listView_Command.SelectedItems[0].Text, this.listView_Command.SelectedItems[0].SubItems[1].Text.Substring(0,2)=="运行"?Tring.Shell:Tring.Key, this.listView_Command.SelectedItems[0].SubItems[1].Text.Substring(0, 2) == "运行"? this.listView_Command.SelectedItems[0].SubItems[1].Text.Substring(3) : this.listView_Command.SelectedItems[0].SubItems[1].Text.Substring(this.listView_Command.SelectedItems[0].SubItems[1].Text.LastIndexOf("+")==-1?3: this.listView_Command.SelectedItems[0].SubItems[1].Text.LastIndexOf("+")+1), this.listView_Command.SelectedItems[0].SubItems[2].Text,
                (this.listView_Command.SelectedItems[0].SubItems[1].Text.Substring(0,2)=="按键" && this.listView_Command.SelectedItems[0].SubItems[1].Text.IndexOf("Ctrl")!=-1),
                (this.listView_Command.SelectedItems[0].SubItems[1].Text.Substring(0, 2) == "按键" && this.listView_Command.SelectedItems[0].SubItems[1].Text.IndexOf("Shift") != -1),
                (this.listView_Command.SelectedItems[0].SubItems[1].Text.Substring(0, 2) == "按键" && this.listView_Command.SelectedItems[0].SubItems[1].Text.IndexOf("Alt") != -1));
            //MessageBox.Show(this.listView_Command.SelectedItems[0].SubItems[1].Text.Substring(0, 2) == "运行" ? this.listView_Command.SelectedItems[0].SubItems[1].Text.Substring(3) : this.listView_Command.SelectedItems[0].SubItems[1].Text.Substring(this.listView_Command.SelectedItems[0].SubItems[1].Text.LastIndexOf("+") == -1 ? 3 : this.listView_Command.SelectedItems[0].SubItems[1].Text.LastIndexOf("+") + 1));
            DialogResult r = finsert.ShowDialog();
            if (r == DialogResult.Yes)
            {
                this.listView_Command.SelectedItems[0].Text = finsert.textBox_key.Text;
                this.listView_Command.SelectedItems[0].SubItems[1].Text = finsert.radioButton_Shell.Checked ? ("运行 " + finsert.textBox_shellexec.Text) : ("按键 " + (finsert.checkBox_Ctrl.Checked ? "Ctrl+" : "") + (finsert.checkBox_Shift.Checked ? "Shift+" : "") + (finsert.checkBox_Alt.Checked ? "Alt+" : "") + finsert.comboBox_key.Text);
                this.listView_Command.SelectedItems[0].SubItems[2].Text = finsert.comboBox_isSearch.Text;
            }
        }
    }
}
