﻿namespace Speech_Recognition
{
    partial class Form_Command
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listView_Command = new System.Windows.Forms.ListView();
            this.cMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cMenu_Insert = new System.Windows.Forms.ToolStripMenuItem();
            this.cMenu_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.cMenu_DeleteAll = new System.Windows.Forms.ToolStripMenuItem();
            this.button = new System.Windows.Forms.Button();
            this.cMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView_Command
            // 
            this.listView_Command.ContextMenuStrip = this.cMenu;
            this.listView_Command.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listView_Command.HideSelection = false;
            this.listView_Command.Location = new System.Drawing.Point(12, 12);
            this.listView_Command.MultiSelect = false;
            this.listView_Command.Name = "listView_Command";
            this.listView_Command.Size = new System.Drawing.Size(420, 400);
            this.listView_Command.TabIndex = 0;
            this.listView_Command.UseCompatibleStateImageBehavior = false;
            this.listView_Command.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView_Command_MouseDoubleClick);
            // 
            // cMenu
            // 
            this.cMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cMenu_Insert,
            this.cMenu_Delete,
            this.cMenu_DeleteAll});
            this.cMenu.Name = "cMenu";
            this.cMenu.Size = new System.Drawing.Size(199, 70);
            this.cMenu.Opening += new System.ComponentModel.CancelEventHandler(this.cMenu_Opening);
            // 
            // cMenu_Insert
            // 
            this.cMenu_Insert.Name = "cMenu_Insert";
            this.cMenu_Insert.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.cMenu_Insert.Size = new System.Drawing.Size(198, 22);
            this.cMenu_Insert.Text = "插入";
            this.cMenu_Insert.Click += new System.EventHandler(this.cMenu_Insert_Click);
            // 
            // cMenu_Delete
            // 
            this.cMenu_Delete.Name = "cMenu_Delete";
            this.cMenu_Delete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.cMenu_Delete.Size = new System.Drawing.Size(198, 22);
            this.cMenu_Delete.Text = "删除";
            this.cMenu_Delete.Click += new System.EventHandler(this.cMenu_Delete_Click);
            // 
            // cMenu_DeleteAll
            // 
            this.cMenu_DeleteAll.Name = "cMenu_DeleteAll";
            this.cMenu_DeleteAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.cMenu_DeleteAll.Size = new System.Drawing.Size(198, 22);
            this.cMenu_DeleteAll.Text = "删除全部";
            this.cMenu_DeleteAll.Click += new System.EventHandler(this.cMenu_DeleteAll_Click);
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(365, 422);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(67, 27);
            this.button.TabIndex = 1;
            this.button.Text = "确定";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // Form_Command
            // 
            this.AcceptButton = this.button;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 461);
            this.Controls.Add(this.button);
            this.Controls.Add(this.listView_Command);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(460, 500);
            this.MinimumSize = new System.Drawing.Size(460, 500);
            this.Name = "Form_Command";
            this.Opacity = 0.8D;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "自定义命令";
            this.Load += new System.EventHandler(this.Form_Command_Load);
            this.cMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView_Command;
        private System.Windows.Forms.Button button;
        private System.Windows.Forms.ContextMenuStrip cMenu;
        private System.Windows.Forms.ToolStripMenuItem cMenu_Insert;
        private System.Windows.Forms.ToolStripMenuItem cMenu_Delete;
        private System.Windows.Forms.ToolStripMenuItem cMenu_DeleteAll;
    }
}