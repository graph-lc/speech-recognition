﻿namespace Speech_Recognition
{
    partial class Form_InsertMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_yes = new System.Windows.Forms.Button();
            this.button_canel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_key = new System.Windows.Forms.TextBox();
            this.textBox_shellexec = new System.Windows.Forms.TextBox();
            this.comboBox_isSearch = new System.Windows.Forms.ComboBox();
            this.radioButton_Shell = new System.Windows.Forms.RadioButton();
            this.radioButton_key = new System.Windows.Forms.RadioButton();
            this.checkBox_Ctrl = new System.Windows.Forms.CheckBox();
            this.checkBox_Shift = new System.Windows.Forms.CheckBox();
            this.checkBox_Alt = new System.Windows.Forms.CheckBox();
            this.comboBox_key = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // button_yes
            // 
            this.button_yes.Location = new System.Drawing.Point(277, 163);
            this.button_yes.Name = "button_yes";
            this.button_yes.Size = new System.Drawing.Size(55, 26);
            this.button_yes.TabIndex = 0;
            this.button_yes.Text = "确定";
            this.button_yes.UseVisualStyleBackColor = true;
            this.button_yes.Click += new System.EventHandler(this.button_yes_Click);
            // 
            // button_canel
            // 
            this.button_canel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_canel.Location = new System.Drawing.Point(216, 163);
            this.button_canel.Name = "button_canel";
            this.button_canel.Size = new System.Drawing.Size(55, 26);
            this.button_canel.TabIndex = 1;
            this.button_canel.Text = "取消";
            this.button_canel.UseVisualStyleBackColor = true;
            this.button_canel.Click += new System.EventHandler(this.button_canel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "关键词";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "识别决策";
            // 
            // textBox_key
            // 
            this.textBox_key.Location = new System.Drawing.Point(62, 28);
            this.textBox_key.Name = "textBox_key";
            this.textBox_key.Size = new System.Drawing.Size(270, 23);
            this.textBox_key.TabIndex = 5;
            // 
            // textBox_shellexec
            // 
            this.textBox_shellexec.Location = new System.Drawing.Point(95, 57);
            this.textBox_shellexec.Name = "textBox_shellexec";
            this.textBox_shellexec.Size = new System.Drawing.Size(237, 23);
            this.textBox_shellexec.TabIndex = 6;
            // 
            // comboBox_isSearch
            // 
            this.comboBox_isSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_isSearch.FormattingEnabled = true;
            this.comboBox_isSearch.Items.AddRange(new object[] {
            "模糊识别",
            "全匹配"});
            this.comboBox_isSearch.Location = new System.Drawing.Point(74, 113);
            this.comboBox_isSearch.Name = "comboBox_isSearch";
            this.comboBox_isSearch.Size = new System.Drawing.Size(258, 25);
            this.comboBox_isSearch.TabIndex = 8;
            // 
            // radioButton_Shell
            // 
            this.radioButton_Shell.AutoSize = true;
            this.radioButton_Shell.Location = new System.Drawing.Point(15, 57);
            this.radioButton_Shell.Name = "radioButton_Shell";
            this.radioButton_Shell.Size = new System.Drawing.Size(74, 21);
            this.radioButton_Shell.TabIndex = 9;
            this.radioButton_Shell.TabStop = true;
            this.radioButton_Shell.Text = "运行程序";
            this.radioButton_Shell.UseVisualStyleBackColor = true;
            this.radioButton_Shell.CheckedChanged += new System.EventHandler(this.radioButton_Shell_CheckedChanged);
            // 
            // radioButton_key
            // 
            this.radioButton_key.AutoSize = true;
            this.radioButton_key.Location = new System.Drawing.Point(15, 86);
            this.radioButton_key.Name = "radioButton_key";
            this.radioButton_key.Size = new System.Drawing.Size(74, 21);
            this.radioButton_key.TabIndex = 10;
            this.radioButton_key.TabStop = true;
            this.radioButton_key.Text = "模拟按键";
            this.radioButton_key.UseVisualStyleBackColor = true;
            this.radioButton_key.CheckedChanged += new System.EventHandler(this.radioButton_key_CheckedChanged);
            // 
            // checkBox_Ctrl
            // 
            this.checkBox_Ctrl.AutoSize = true;
            this.checkBox_Ctrl.Location = new System.Drawing.Point(94, 86);
            this.checkBox_Ctrl.Name = "checkBox_Ctrl";
            this.checkBox_Ctrl.Size = new System.Drawing.Size(47, 21);
            this.checkBox_Ctrl.TabIndex = 11;
            this.checkBox_Ctrl.Text = "Ctrl";
            this.checkBox_Ctrl.UseVisualStyleBackColor = true;
            // 
            // checkBox_Shift
            // 
            this.checkBox_Shift.AutoSize = true;
            this.checkBox_Shift.Location = new System.Drawing.Point(147, 86);
            this.checkBox_Shift.Name = "checkBox_Shift";
            this.checkBox_Shift.Size = new System.Drawing.Size(52, 21);
            this.checkBox_Shift.TabIndex = 12;
            this.checkBox_Shift.Text = "Shift";
            this.checkBox_Shift.UseVisualStyleBackColor = true;
            // 
            // checkBox_Alt
            // 
            this.checkBox_Alt.AutoSize = true;
            this.checkBox_Alt.Location = new System.Drawing.Point(205, 86);
            this.checkBox_Alt.Name = "checkBox_Alt";
            this.checkBox_Alt.Size = new System.Drawing.Size(42, 21);
            this.checkBox_Alt.TabIndex = 13;
            this.checkBox_Alt.Text = "Alt";
            this.checkBox_Alt.UseVisualStyleBackColor = true;
            // 
            // comboBox_key
            // 
            this.comboBox_key.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_key.FormattingEnabled = true;
            this.comboBox_key.Location = new System.Drawing.Point(253, 84);
            this.comboBox_key.Name = "comboBox_key";
            this.comboBox_key.Size = new System.Drawing.Size(79, 25);
            this.comboBox_key.TabIndex = 14;
            // 
            // Form_InsertMessage
            // 
            this.AcceptButton = this.button_yes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button_canel;
            this.ClientSize = new System.Drawing.Size(344, 201);
            this.ControlBox = false;
            this.Controls.Add(this.comboBox_key);
            this.Controls.Add(this.checkBox_Alt);
            this.Controls.Add(this.checkBox_Shift);
            this.Controls.Add(this.checkBox_Ctrl);
            this.Controls.Add(this.radioButton_key);
            this.Controls.Add(this.radioButton_Shell);
            this.Controls.Add(this.comboBox_isSearch);
            this.Controls.Add(this.textBox_shellexec);
            this.Controls.Add(this.textBox_key);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_canel);
            this.Controls.Add(this.button_yes);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(360, 240);
            this.MinimumSize = new System.Drawing.Size(360, 240);
            this.Name = "Form_InsertMessage";
            this.Opacity = 0.8D;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "插入命令";
            this.Load += new System.EventHandler(this.Form_InsertMessage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_yes;
        private System.Windows.Forms.Button button_canel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox textBox_key;
        public System.Windows.Forms.TextBox textBox_shellexec;
        public System.Windows.Forms.ComboBox comboBox_isSearch;
        public System.Windows.Forms.ComboBox comboBox_key;
        public System.Windows.Forms.RadioButton radioButton_Shell;
        public System.Windows.Forms.RadioButton radioButton_key;
        public System.Windows.Forms.CheckBox checkBox_Ctrl;
        public System.Windows.Forms.CheckBox checkBox_Shift;
        public System.Windows.Forms.CheckBox checkBox_Alt;
    }
}